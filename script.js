// 1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//Прототипне наслідування значить що створюються один об'ект як приклад (прототип) та можна створювани на основі його об'єкти додаючи або перезаписуючи функціонал.

// 2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
//super() викликається у контрукторі класу-нащадка для того щоб виконати його батьківський конструктор щоб переписати об'єкт this інакше його не буде створено. 

class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }
    setName (value) {
        this.name = value
    }
    getName () {
        return this.name
    }
    setAge (value) {
        this.age=value
    }
    getAge () {
        return this.age
    }
    setSalary (value) {
        this.salary=value
    }
    getSalary() {
        return this.salary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)

        this.lang = lang
    }
    setLang (value) {
        this.lang = value
    }
    getLang () {
        return this.lang
    }

    getSalary () {
        return this.salary * 3
    }
}

let programmer1 = new Programmer ('Alex', '33', '3000', 'JS');
let programmer2 = new Programmer ('Andrea', '40', '2000', 'Phyton');
let programmer3 = new Programmer ('Atilio', '49', '10000', 'JAVA');

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);


console.log(programmer1.getSalary());
console.log(programmer2.getSalary());
console.log(programmer3.getSalary());